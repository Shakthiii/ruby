require 'nokogiri'
require 'csv'
# require 'byebug'

class MySaxParser < Nokogiri::XML::SAX::Document
  def initialize
    # Initialize 
    @cur_element = nil
    @event_data = {}
    @FINAL_ARRAY = []
  end

  def start_element(name, attrs = [])
    # byebug
    # Handle the start of each element
    @cur_element = name
    if attrs.empty?
      @event_data[@cur_element] = ''
    else
      @event_data[@cur_element] = {}
      attrs.each do |attr|
        # Add the attributes to the @event_data hash
        # byebug
        @event_data[@cur_element][attr[0]] = attr[1]
      end
    end
  end

  def characters(string)
    # byebug
    string = string.strip
    # Handle the character data between the start and end tags of an element
    if @event_data[@cur_element].is_a?(String) && string!=""
      # If @event_data[@cur_element] is a string, append the character data to it
      @event_data[@cur_element] << string if @cur_element 
    elsif @event_data[@cur_element].is_a?(Hash) && string!=""
      # If @event_data[@cur_element] is a hash, add the character data as the value for the key "content"
      @event_data[@cur_element] = string
    end
  end

  def end_element(name)
    # byebug
    # Handle at the end of an element
    case name
    when 'EventData'
    #   puts @event_data
      # Extract the @event_data hash and write to the CSV file
      if @event_data['EventData']['eventType'] == 'Primary'
        # For Counting Events occurs in the loop
        # @count +=1

        # Media type
        value = @event_data['EventData']['eventType'].strip

        # Hour
        hourDate = @event_data['SmpteDateTime']['broadcastDate'].strip
        hourTime = @event_data['SmpteTimeCode'].strip
        hour = hourDate + ' ' + hourTime
        
        # duration
        # byebug
        duration = @event_data['SmpteTimeCode'].strip
        # puts duration

        # segement Id
        segement_id = @event_data['SegmentNumber'].strip

        # segement name
        segement_Name = @event_data['Channel']['shortName'].strip

        # for title
        title = @event_data['EventTitle'].strip

        # asset Id
        asset_id = @event_data['EventId'].strip

        # Storing the Array
        @FINAL_ARRAY << [hour, duration, '', segement_id, asset_id, 'Promo', value, title, '', '', '', '', '', '', '', '', '', ""]

        # Spliting it for the row 1
        hourToAddInFirst = @FINAL_ARRAY[0][0].split(' ')

        # Add values to the CSV file
        CSV.open('./res/newfile.csv', 'w') do |csv|
            csv << [hourToAddInFirst[0], hourToAddInFirst[1], "", "CHANNEL NAME",segement_Name]
            csv << ['START TIME', 'DURATION', 'OFFSET', 'SEGMENT ID', 'ASSET ID', 'TYPE', 'MEDIA TYPE', 'TITLE', 'SUBTITLE_ID', 'COMMENTS', 'RECONCILE_ID', 'START TIME TYPE', 'AUDIOS_EXPECTED', 'SUBTITLES_EXPECTED', 'AUDIO_RULE', 'SUBTITLE_RULE', 'SHOW METADATA']
            @FINAL_ARRAY.each do |row|
                csv << row
            end
        end
        puts 'Done writing to CSV file.'
      end
    end
  end 
end
# Parse the XML data 
parser = Nokogiri::XML::SAX::Parser.new(MySaxParser.new)
parser.parse(File.read('./sample_playlist.xml'))
