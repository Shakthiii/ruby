require 'csv'
require 'nokogiri'

class Cusomter < Nokogiri::XML::SAX::Document
  def initialize
    @currentRow = []
    @allRows = []
  end

  def start_element(name, attrs=[])
    if name == 'Customer'
      @currentRow = []
    elsif name == "FullAddress"
      @Address = true
      @FullAddress = []
    else
    @currentName = name
    end
  end

  def characters(text) 
    text = text.strip
    return if text.empty?|| @currentName == 'Fax'
    if @Address 
      @FullAddress << text
    else
      @currentRow << text
    end
  end

  def end_element(name)
    if name == 'Customer'
      @allRows << @currentRow
    elsif name == "Customers"
      convertToCsv(@allRows)
    elsif name == "FullAddress"
      @Address = false
      @currentRow << @FullAddress.join(', ')
    end
  end

  def convertToCsv(data)
    CSV.open('./res/Output.csv', 'w') do |csv|
      puts "CSV file printed"
      csv << ['CompanyName', 'ContactName', 'ContactTitle', 'Phone', 'FullAddress']
      data.each do |row|
        csv << row
      end
    end
  end

end

parser = Nokogiri::XML::SAX::Parser.new(Cusomter.new)
parser.parse(File.open('./sampleData.xml'))


