require 'rubyXL'

class ExcelExtractor
  attr_reader :feed_id, :start_date, :end_date
  def initialize(feed_id, start_date, end_date)
    @feed_id = feed_id
    @start_date = start_date
    @end_date = end_date
  end

#this method to extract the file path and compare the values from the input 
  def extract(filePath)
    # empty hash 
    data = {}
    csvFile = RubyXL::Parser.parse(filePath)
    worksheet = csvFile[0]
    worksheet.each do |row, row_index|
# filtering the feed_id by using row_index
      next unless row[4].value == feed_id
      start_time = DateTime.parse(row[0].value)
      end_time = DateTime.parse(row[1].value)
#comparing the start_time and end_time
#start time should be greater than end time or lesser than start date
      next if end_date < start_time || end_time < start_date
      asset_types = row[2].value.split(',')
      logo_asset_id = row[3].value
#loop over the asset_types
      asset_types.each do |type|
        type = type.strip
        data[type] ||= []
        # checking if already logo asset present or not.
        logoPresent = data[type].any? do |value|
          value['logo_asset_id'] == logo_asset_id
        end
        unless logoPresent
        # push to the hash if the condition pass
          data[type] << {
            'start_time' => start_time.to_s,
            'end_time' => end_time.to_s,
            'logo_asset_id' => logo_asset_id
          }
        end
      end
    end
    # if we did'nt metion it its shows the undefined symbol
    data
  end
end

# input data
extractor = ExcelExtractor.new('TAMTR', DateTime.parse('2022-09-25 06:00:00:00'), DateTime.parse('2022-09-26 06:00:00:00'))
data = extractor.extract('./sample.xlsx')
puts data


final = {
    "Series" => [
      {
        "start_time" => "2022-09-25 00:00:00:00",
        "end_time" => "2022-09-25 10:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_032522"
      },
      {
        "start_time" => "2022-09-24 06:00:00:00",
        "end_time" => "2022-09-27 06:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_052522"
      },
    ],
    "Autolowerthird" => [
      {
        "start_time" => "2022-09-25 06:00:00:00",
        "end_time" => "2022-09-25 12:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_042522"
      },
      {
        "start_time" => "2022-09-24 06:00:00:00",
        "end_time" => "2022-09-27 06:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_052522"
      },
    ],
    "media" => [
      {
        "start_time" => "2022-09-25 06:00:00:00",
        "end_time" => "2022-09-25 12:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_042522"
      },
      {
        "start_time" => "2022-09-24 06:00:00:00",
        "end_time" => "2022-09-27 06:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_072522"
      },
    ],
    "Ecomm" => [
      {
        "start_time" => "2022-09-24 06:00:00:00",
        "end_time" => "2022-09-27 06:00:00:00",
        "logo_asset_id" => "TM_Wordmark_logo_072522"
      },
    ],
  }


puts data == final 
