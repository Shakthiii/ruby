input = "|Title1=COMING UP...|Category1=Line1|Title2=09:20|
Category2=Line2|Title3=One Stop Science Shop|
Category3=Line3|Title4=09:30|Category4=Line4|
Title5=My Petsaurus|Category5=Line5|Title6=09:35|
Category6=Line6|Title7=nil|
Category7=Line7"

def transformString(input)
    # deleting the new line breaks
    input = input.delete("\n")
    #Creating the empty hash
    output = {} 
    #Using regualar expression to filter out the expected value
    input.scan(/Title\d+=(.*?)\|Category(\d+)=(.*?)\|/).each do |title, value, category|
        next if title == nil
        output[category] = title
    end
    # puts output
    output.map { |key, value| "#{key}=#{value}" }.join("&")
end

output = transformString(input)
#Printing the output value
puts output
final = "Line1=COMING UP...&Line2=09:20&Line3=One Stop Science Shop&Line4=09:30&Line5=My Petsaurus&Line6=09:35"
#Checkeing whether the output value is equal to final
puts output == final


